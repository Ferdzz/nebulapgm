package in.twizmwaz.cardinal.rank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class Rank {

    private static List<Rank> ranks = new ArrayList<>();
    private static HashMap<UUID, List<Rank>> playerRanks = new HashMap<>();
    private String name;
    private String flair;
    private boolean staffRank;
    private boolean defaultRank;

    public Rank(String name, String flair, boolean staffRank, boolean defaultRank) {
        this.name = name;
        this.flair = flair;
        this.staffRank = staffRank;
        this.defaultRank = defaultRank;

        ranks.add(this);
    }

    public static List<Rank> getRanks() {
        return ranks;
    }

    public static List<Rank> getDefaultRanks() {
        List<Rank> results = new ArrayList<>();
        for (Rank rank : ranks) {
            if (rank.isDefaultRank()) {
                results.add(rank);
            }
        }
        return results;
    }

    public static List<Rank> getRanksByPlayer(UUID player) {
        return playerRanks.containsKey(player) ? playerRanks.get(player) : new ArrayList<Rank>();
    }

    public static String getPlayerPrefix(UUID player) {
        String prefix = "";
        String staffChar = "\u2756";
        String donateChar = "\u274B";
        Permission perms = Bukkit.getServer().getServicesManager().getRegistration(Permission.class).getProvider();
    	String rank = perms.getPrimaryGroup(Bukkit.getPlayer(player));
    	
        if(rank.equals("moderator") || rank.equals("admin"))
        	prefix += ChatColor.GOLD + staffChar;
        else if (rank.equals("elite"))
        	prefix += ChatColor.DARK_PURPLE + donateChar;
        else if (rank.equals("mvp"))
        	prefix += ChatColor.YELLOW + donateChar;
        else if (rank.equals("vip"))
        	prefix += ChatColor.GREEN + donateChar;
        return prefix;
    }

    public String getName() {
        return name;
    }

    public String getFlair() {
        return flair;
    }

    public boolean isStaffRank() {
        return staffRank;
    }

    public boolean isDefaultRank() {
        return defaultRank;
    }

    public void addPlayer(UUID player) {
        if (!playerRanks.containsKey(player)) {
            playerRanks.put(player, new ArrayList<Rank>());
        }
        playerRanks.get(player).add(this);
    }

    public void removePlayer(UUID player) {
        if (!playerRanks.containsKey(player)) {
            playerRanks.put(player, new ArrayList<Rank>());
        }
        playerRanks.get(player).remove(this);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (Rank rank : getDefaultRanks()) {
            rank.addPlayer(event.getPlayer().getUniqueId());
        }
//        Bukkit.getPluginManager().callEvent(new RankChangeEvent());
    }

}
