package in.twizmwaz.cardinal.util;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class PlayerUtils {

    public static void resetPlayer(Player player) {
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setSaturation(20);
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
        for (PotionEffect effect : player.getActivePotionEffects()) {
            try {
                player.removePotionEffect(effect.getType());
            } catch (NullPointerException ignored) {
            }
        }
        player.setTotalExperience(0);
        player.setExp(0);
        player.setPotionParticles(true);
        player.setWalkSpeed(0.2F);
        player.setFlySpeed(0.2F);
    }

    public static double getSnowflakeMultiplier(OfflinePlayer player) {
    	Permission perms = Bukkit.getServer().getServicesManager().getRegistration(Permission.class).getProvider();
    	String rank = perms.getPrimaryGroup(player.getPlayer());
        if(rank.equals("elite"))
        	return 2.5d;
        if(rank.equals("mvp"))
        	return 2d;
        if(rank.equals("vip"))
        	return 1.5d;
        else
        	return 1;
    }

    public static String getColoredName(OfflinePlayer player) {
        if (player.isOnline())
            return player.getPlayer().getDisplayName();
        return ChatColor.DARK_AQUA + player.getName();
    }

}
