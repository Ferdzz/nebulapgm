package in.twizmwaz.cardinal.module.modules.updateNotification;

import in.twizmwaz.cardinal.module.Module;

import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

public class UpdateNotification implements Module {

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {
	}

}