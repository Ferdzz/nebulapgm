package in.twizmwaz.cardinal.command;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

import in.twizmwaz.cardinal.Cardinal;
import in.twizmwaz.cardinal.event.SnowflakeChangeEvent;
import in.twizmwaz.cardinal.module.modules.snowflakes.Snowflakes.ChangeReason;
import in.twizmwaz.cardinal.util.TeamUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SnowflakesCommand {

	@Command(aliases = { "atom", "atoms" }, desc = "View your own or another player's snowflake count.", usage = "[player]")
	public static void settings(final CommandContext cmd, CommandSender sender) throws CommandException {
		if (cmd.argsLength() == 0) {
			Bukkit.dispatchCommand(sender, "atom " + sender.getName());
		} else if (cmd.argsLength() == 1 && (cmd.getString(0).equalsIgnoreCase("send") || (cmd.getString(0).equalsIgnoreCase("give") && sender.isOp()))) { // Describes the command usage
			if(cmd.getString(0).equalsIgnoreCase("send")) {
				sender.sendMessage(ChatColor.DARK_PURPLE + "Transfers atoms from one player to another");
				sender.sendMessage(ChatColor.DARK_PURPLE + "Usage: " + ChatColor.GOLD + "/atoms send [amount] [player]");
			} else {
				sender.sendMessage(ChatColor.DARK_PURPLE + "Spawns new atoms to give to a player");
				sender.sendMessage(ChatColor.DARK_PURPLE + "Usage: " + ChatColor.GOLD + "/atoms give [amount] [player]");
			}
		} else if (cmd.argsLength() == 1) {
			String name = Bukkit.getOfflinePlayer(cmd.getString(0)).getName();
			sender.sendMessage(ChatColor.DARK_PURPLE + (sender.getName().equals(name) ? "You have " : TeamUtils.getTeamColorByPlayer(Bukkit.getOfflinePlayer(name)) + name + ChatColor.DARK_PURPLE + " has ") + ChatColor.GOLD + (Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(cmd.getString(0)), "snowflakes").equals("") ? "0" : Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(cmd.getString(0)), "snowflakes")) + " atom" + (Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(cmd.getString(0)), "snowflakes").equals("1") ? "" : "s"));
			return;
		} else if (sender instanceof Player && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("send") && Integer.parseInt(Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(sender.getName()), "snowflakes").equals("") ? "0" : Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(sender.getName()), "snowflakes")) >= Integer.parseInt(cmd.getString(1)) && Integer.parseInt(cmd.getString(1)) > 0) {
			int sent = Integer.parseInt(cmd.getString(1));
			Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent((Player) sender, ChangeReason.PAID_GIVEN, -sent, cmd.getString(2)));
			Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent(Bukkit.getOfflinePlayer(cmd.getString(2)).getPlayer(), ChangeReason.PAID_RECEIVED, sent, sender.getName()));
			return;
		} else if (sender instanceof Player && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("send") && !(Integer.parseInt(Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(sender.getName()), "snowflakes").equals("") ? "0" : Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(sender.getName()), "snowflakes")) >= Integer.parseInt(cmd.getString(1))) || !(Integer.parseInt(cmd.getString(1)) > 0)) {
			sender.sendMessage(ChatColor.RED + "Insufficient atoms");
			return;
		} else if (sender.isOp() && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("give")) {
			int sent = Integer.parseInt(cmd.getString(1));
			Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent(Bukkit.getOfflinePlayer(cmd.getString(2)).getPlayer(), ChangeReason.PAID_RECEIVED, sent, sender.getName()));
		}
	}

}
