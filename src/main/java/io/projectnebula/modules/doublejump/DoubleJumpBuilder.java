package io.projectnebula.modules.doublejump;

import org.bukkit.Bukkit;

import in.twizmwaz.cardinal.Cardinal;
import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class DoubleJumpBuilder implements ModuleBuilder {

	@Override
	public ModuleCollection<DoubleJump> load(Match match) {
		ModuleCollection<DoubleJump> results = new ModuleCollection<DoubleJump>();
		if(match.getDocument().getRootElement().getChild("doublejump") != null) {
			DoubleJump dj = new DoubleJump();
			if(match.getDocument().getRootElement().getChild("doublejump").getChild("height") != null) {
				dj.height = Double.parseDouble(match.getDocument().getRootElement().getChild("doublejump").getChildText("height"));
			}
			if(match.getDocument().getRootElement().getChild("doublejump").getChild("velocity") != null) {
				dj.velocity = Double.parseDouble(match.getDocument().getRootElement().getChild("doublejump").getChildText("velocity"));
			}
			Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Cardinal.getInstance(), dj, 0L, 5L);
			results.add(dj);
		}
		return results;
	}

}
