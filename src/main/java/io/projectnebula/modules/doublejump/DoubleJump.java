package io.projectnebula.modules.doublejump;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.match.MatchState;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.util.TeamUtils;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class DoubleJump implements Module, Runnable{

	//Default values
	public double height = 0.7;
	public double velocity = 1.3f;
	
	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		Player player = event.getPlayer();
		if (player.getGameMode() == GameMode.CREATIVE || TeamUtils.getTeamByPlayer(player).isObserver())
			return;
		event.setCancelled(true);
		player.setFlying(false);
		player.setAllowFlight(false);
		player.setVelocity(player.getVelocity().multiply(velocity).setY(height));
		player.setExp(0);
	}

	@EventHandler
	public void onPlayerJump(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (!player.getGameMode().equals(GameMode.CREATIVE) && player.getExp() >= 1 && !player.isFlying() && !TeamUtils.getTeamByPlayer(player).isObserver()) {
			player.setAllowFlight(true);
		}
	}
	
	//Increments xp
	@Override
	public void run() {
		for (Player player: Bukkit.getServer().getOnlinePlayers()) {
			if(!TeamUtils.getTeamByPlayer(player).isObserver() && player.getExp() <= 1 && GameHandler.getGameHandler().getMatch().getState().equals(MatchState.PLAYING)) {
				player.setExp(player.getExp() + 0.05F);
			}
		}
	}
}
