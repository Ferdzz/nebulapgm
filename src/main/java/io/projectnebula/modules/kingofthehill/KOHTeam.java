package io.projectnebula.modules.kingofthehill;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import in.twizmwaz.cardinal.module.modules.team.TeamModule;


@SuppressWarnings("rawtypes")

public class KOHTeam {

	private TeamModule team;
	private ArrayList<Player> playersOnHill;
	private double score;
	private boolean fullyCaptured;
	
	public KOHTeam(TeamModule team){
		this.team = team;
		score = 0;
		playersOnHill = new ArrayList<Player>();
	}
	
	public TeamModule getTeam(){
		return team;
	}
	
	public double getScore(){
		return score;
	}
	
	public boolean isFullyCaptured(){
		return fullyCaptured;
	}
	
	public ArrayList<Player> getPlayersOnHill(){
		return playersOnHill;
	}
	
	public void setFullyCaptured(boolean val){
		fullyCaptured = val;
	}
	
	public void addScore(double amount){
		score += amount;
	}
	
	public void addPlayerToHill(Player pl){
		if(!playersOnHill.contains(pl)){
			playersOnHill.add(pl);
		}
	}
	
	public void removePlayerFromHill(Player pl){
		if(playersOnHill.contains(pl)){
			playersOnHill.remove(pl);
		}
	}
}
