package io.projectnebula.modules.kingofthehill;

import org.bukkit.Location;
import org.jdom2.Element;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class KingOfTheHillBuilder implements ModuleBuilder {

	@SuppressWarnings("rawtypes")
	@Override
	public ModuleCollection load(Match match) {
		ModuleCollection<KingOfTheHill> results = new ModuleCollection<KingOfTheHill>();
		for (Element hills : match.getDocument().getRootElement().getChildren("kingofthehill")) {
			Location beaconLoc = null;
			double rad = 0;
			for (Element locs : hills.getChildren("beaconloc")) {
				String[] coord = locs.getText().split(",");
				double x = Double.parseDouble(coord[0]);
				double y = Double.parseDouble(coord[1]);
				double z = Double.parseDouble(coord[2]);
				beaconLoc = new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z);
			}
			for (Element rads : hills.getChildren("rad")) {
				rad = Double.parseDouble(rads.getText());
			}
			results.add(new KingOfTheHill(beaconLoc, rad));
		}
		return results;
	}

}
