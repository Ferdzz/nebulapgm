package io.projectnebula.modules.kingofthehill;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.event.MatchStartEvent;
import in.twizmwaz.cardinal.event.SnowflakeChangeEvent;
import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.module.modules.snowflakes.Snowflakes;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.modules.scoreboard.NebulaScoreboard;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sk89q.minecraft.util.commands.ChatColor;

public class KingOfTheHill implements Module {

	private Location beaconLoc;
	private int lastWinningIndex;
	private int indexOnHill;
	@SuppressWarnings("unused")
	private double hillRad;
	private double leftX;
	private double rightX;
	private double topY;
	private double btmY;
	private double timeForOneRow;
	private int captureID;
	
	public KingOfTheHill(Location beaconLoc, double hillRad){
		this.beaconLoc = beaconLoc;
		this.hillRad = hillRad;
		lastWinningIndex = -1;
		indexOnHill = -1;
		leftX = beaconLoc.getX() - hillRad;
		rightX = beaconLoc.getX() + hillRad;
		topY = beaconLoc.getZ() + hillRad;
		btmY = beaconLoc.getZ() - hillRad;
		timeForOneRow = (10/(hillRad+1));
	}
	
	@SuppressWarnings("deprecation")
	private void changeBlocks(String team){
		byte woolColor = (byte)0;
		
		switch(team.toLowerCase().replace('_', ' ')){
			case "dark red":
				woolColor = (byte)14;
				break;
			case "red":
				woolColor = (byte)14;
				break;
			case "blue":
				woolColor = (byte)11;
				break;
			case "dark aqua":
				woolColor = (byte)9;
				break;
			case "aqua":
				woolColor = (byte)3;
				break;
			case "dark blue":
				woolColor = (byte)11;
				break;
			case "yellow":
				woolColor = (byte)4;
				break;
			case "gold":
				woolColor = (byte)1;
				break;
			case "dark green":
				woolColor = (byte)13;
				break;
			case "green":
				woolColor = (byte)5;
				break;
			case "dark purple":
				woolColor = (byte)10;
				break;
			case "light purple":
				woolColor = (byte)6;
				break;
			case "white":
				woolColor = (byte)8;
				break;
			case "gray":
				woolColor = (byte)8;
				break;
			case "dark gray":
				woolColor = (byte)7;
				break;
			case "black":
				woolColor = (byte)15;
				break;
			default:
				woolColor = (byte)0;
		}
		
		final byte finalWool = woolColor;
		
		/*for(double y=btmY; y<=topY; y++){
			for(double x=leftX; x<=rightX; x++){
				Block bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(new Location(beaconLoc.getWorld(), x, beaconLoc.getY()-1, y));
				bl.setType(Material.WOOL);
				bl.setData(woolColor);
			}
		}*/
		
		Bukkit.getScheduler().cancelTask(captureID);
		captureID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GameHandler.getGameHandler().getPlugin(), new Runnable() {
			double y = btmY;		
			@Override
			public void run() {
				for(double x=leftX; x<=rightX; x++){
					Bukkit.getServer().getPluginManager().callEvent(new KOHChangeWoolEvent(x, y, finalWool));
				}
				y++;
			}
		}, 0L, (long)(20*timeForOneRow));
	}
	
	private void updateHill(){
		int index = getTeamOnHillIndex();
		if(index != lastWinningIndex && index != -1){
			//Bukkit.getScheduler().cancelTask(captureID);
			for(Player pl:Bukkit.getOnlinePlayers()){
				pl.sendMessage(NebulaScoreboard.getTeams().get(index).getTeam().getColor() + NebulaScoreboard.getTeams().get(index).getTeam().getName() + ChatColor.GREEN + " now has the hill!");
				changeBlocks(NebulaScoreboard.getTeams().get(index).getTeam().getColor().name());
			}
			lastWinningIndex = index;
			return;
		}
		if(index==-1){
			//Bukkit.getScheduler().cancelTask(captureID);
			if(lastWinningIndex != -1){
				NebulaScoreboard.getTeams().get(lastWinningIndex).setFullyCaptured(false);
			}	
			for(Player pl:Bukkit.getOnlinePlayers()){			
				if(lastWinningIndex != -1){
					pl.sendMessage(NebulaScoreboard.getTeams().get(lastWinningIndex).getTeam().getColor() + NebulaScoreboard.getTeams().get(lastWinningIndex).getTeam().getName() + ChatColor.RED + " lost the hill!");
				}		
				changeBlocks("noone");
			}
			lastWinningIndex = -1;
		}
	}
	
	private int getTeamOnHillIndex(){
		int biggestSize = 0;
		if(lastWinningIndex != -1){
			biggestSize = NebulaScoreboard.getTeams().get(lastWinningIndex).getPlayersOnHill().size();
		}
		int index = -1;
		boolean same = false;
		boolean stopSameSearch = false;
		for(int x=0; x<NebulaScoreboard.getTeams().size(); x++){
			if(!stopSameSearch && x>0 && NebulaScoreboard.getTeams().get(x-1).getPlayersOnHill().size() == NebulaScoreboard.getTeams().get(x).getPlayersOnHill().size()){
				same = true;
			}else if(x>0){
				same = false;
				stopSameSearch = true;
			}
			if(NebulaScoreboard.getTeams().get(x).getPlayersOnHill().size() > biggestSize && NebulaScoreboard.getTeams().get(x).getPlayersOnHill().size() > 0 && NebulaScoreboard.getTeams().get(x).getPlayersOnHill().size() != biggestSize){
				biggestSize = NebulaScoreboard.getTeams().get(x).getPlayersOnHill().size();
				index = x;
			}
		}
		if(same){
			index = -1;
		}
		if(index == -1 && lastWinningIndex != -1){
			NebulaScoreboard.getTeams().get(lastWinningIndex).setFullyCaptured(false);
		}
		return index;
	}
	
	private int getIndexOfKOHTeamWithPlayer(Player pl){
		for(int x=0; x<NebulaScoreboard.getTeams().size(); x++){
			if(NebulaScoreboard.getTeams().get(x).getTeam().equals(TeamUtils.getTeamByPlayer(pl))){
				return x;
			}
		}
		return -1;
	}
	
	private boolean isPlayerOnHill(Player pl){
		return Math.floor(pl.getLocation().getX()) >= leftX && Math.floor(pl.getLocation().getX()) <= rightX 
				&& Math.floor(pl.getLocation().getZ()) <= topY && Math.floor(pl.getLocation().getZ()) >= btmY;
	}
	
	@EventHandler
	public void onStep(PlayerMoveEvent e){	
		Match match = GameHandler.getGameHandler().getMatch();
		if(match.isRunning() && !TeamUtils.getTeamByPlayer(e.getPlayer()).getName().equals("Observers") && isPlayerOnHill(e.getPlayer()) && !NebulaScoreboard.getTeams().get(getIndexOfKOHTeamWithPlayer(e.getPlayer())).getPlayersOnHill().contains(e.getPlayer())){
			NebulaScoreboard.getTeams().get(getIndexOfKOHTeamWithPlayer(e.getPlayer())).addPlayerToHill(e.getPlayer());
			indexOnHill = getTeamOnHillIndex();
			updateHill();
		}else if(match.isRunning() && !TeamUtils.getTeamByPlayer(e.getPlayer()).getName().equals("Observers") && !isPlayerOnHill(e.getPlayer()) && NebulaScoreboard.getTeams().get(getIndexOfKOHTeamWithPlayer(e.getPlayer())).getPlayersOnHill().contains(e.getPlayer())){
			NebulaScoreboard.getTeams().get(getIndexOfKOHTeamWithPlayer(e.getPlayer())).removePlayerFromHill(e.getPlayer());
			indexOnHill = getTeamOnHillIndex();
			updateHill();
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onWoolChange(KOHChangeWoolEvent e){
		if(e.getY() > topY){
			if(indexOnHill != -1){
				NebulaScoreboard.getTeams().get(indexOnHill).setFullyCaptured(true);
			}
			Bukkit.getScheduler().cancelTask(captureID);
		}else{
			Block bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(new Location(beaconLoc.getWorld(), e.getX(), beaconLoc.getY()-1, e.getY()));
			bl.setType(Material.WOOL);
			bl.setData(e.getWoolColor());
		}
	}
	
	@EventHandler
	public void onStart(MatchStartEvent e){
		for(double y=btmY; y<=topY; y++){
			for(double x=leftX; x<=rightX; x++){
				Block bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(new Location(beaconLoc.getWorld(), x, beaconLoc.getY()-1, y));
				if(!bl.getType().equals(Material.BEACON)){
					bl.setType(Material.WOOL);
					bl.setData((byte)0);
				}
			}
	    }
		lastWinningIndex = -1;
		indexOnHill = -1;
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(GameHandler.getGameHandler().getPlugin(), new Runnable() {
			@Override
			public void run() {
				int index = indexOnHill;
				if(index != -1 && GameHandler.getGameHandler().getMatch().isRunning() && NebulaScoreboard.getTeams().get(index).isFullyCaptured()){
					NebulaScoreboard.getScoreboard().addScore(NebulaScoreboard.getTeams().get(index), 5.0);
					for(Player pl:NebulaScoreboard.getTeams().get(index).getPlayersOnHill()){
						Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent(pl, Snowflakes.ChangeReason.HILL_CAPTURE, 5, ""));
					}
				}
			}
		}, 0L, 20L);
	}

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}
	
}
