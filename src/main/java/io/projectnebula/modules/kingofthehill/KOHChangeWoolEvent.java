package io.projectnebula.modules.kingofthehill;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class KOHChangeWoolEvent extends Event implements Cancellable{
	
    private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	private double x;
	private double y;
	private byte woolColor;
	
	public KOHChangeWoolEvent(double x, double y, byte woolColor){
		this.x = x;
		this.y = y;
		this.woolColor = woolColor;
	}

	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public byte getWoolColor(){
		return woolColor;
	}
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		cancelled = arg0;	
	}

	public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
	

}
