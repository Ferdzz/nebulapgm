package io.projectnebula.modules.itemRightClick;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.command.HatCommand;
import io.projectnebula.command.TrailCommand;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemRightClick implements Module {

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void InventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();  
		if(e.getInventory().getTitle().contains("Trails")){
			e.setCancelled(true);
			if(e.getCurrentItem() == null){
				return;
			}else{
				if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Remove Trail")){
					Bukkit.dispatchCommand(p, "trail remove");
					p.closeInventory();

				}else{
					Bukkit.dispatchCommand(p, "trail " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.closeInventory();
				}
			}
       }else if(e.getInventory().getTitle().contains("Hats")){
    	   e.setCancelled(true);
    	   if(e.getCurrentItem() == null){
    		   return;
    	   }else{
    		   if(e.getCurrentItem().getItemMeta().getDisplayName() != null &&  e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Go To Page 2")){
    			   p.closeInventory();
				   Inventory page2 = Bukkit.createInventory(null,54, "Hats");
				   for(Material mat: HatCommand.page2){
						ItemStack stack = new ItemStack(mat);
						ItemMeta meta = stack.getItemMeta();
						List<String> lore = Arrays.asList(mat.name());
						meta.setLore(lore);
						stack.setItemMeta(meta);
						page2.addItem(stack);
						
				   }
				   ItemStack next = new ItemStack(Material.ARROW);
				   ItemMeta next_meta = next.getItemMeta();
				   next_meta.setDisplayName("Go To Page 3");
				   next.setItemMeta(next_meta);
				   page2.setItem(53,next);
				   ItemStack back = new ItemStack(Material.ARROW);
				   ItemMeta back_meta = back.getItemMeta();
				   back_meta.setDisplayName("Go To Page 1");
				   back.setItemMeta(back_meta);
				   page2.setItem(52,back);
				   p.openInventory(page2);
				 
    		   }else if(e.getCurrentItem().getItemMeta().getDisplayName() != null && e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Go To Page 3")){
    			   p.closeInventory();
    			   Inventory page3 = Bukkit.createInventory(null,54, "Hats"); 
					for(Material mat: HatCommand.page3){
						ItemStack stack = new ItemStack(mat);
						ItemMeta meta = stack.getItemMeta();
						List<String> lore = Arrays.asList(mat.name());
						meta.setLore(lore);
						stack.setItemMeta(meta);
						page3.addItem(stack);
						
				   }
				   ItemStack back = new ItemStack(Material.ARROW);
				   ItemMeta back_meta = back.getItemMeta();
				   back_meta.setDisplayName("Go To Page 2");
				   back.setItemMeta(back_meta);
				   page3.setItem(52,back);
				   p.openInventory(page3);
    		   }else if(e.getCurrentItem().getItemMeta().getDisplayName() != null && e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Go To Page 1")){
    			   p.closeInventory();
    			   Inventory inv = Bukkit.createInventory(null,54, "Hats");
    			   for(Material mat: HatCommand.page1){
						ItemStack stack = new ItemStack(mat);
						ItemMeta meta = stack.getItemMeta();
						List<String> lore = Arrays.asList(mat.name());
						meta.setLore(lore);
						stack.setItemMeta(meta);
						inv.addItem(stack);
						
				    }
					ItemStack next = new ItemStack(Material.ARROW);
					ItemMeta next_meta = next.getItemMeta();
					next_meta.setDisplayName("Go To Page 2");
					next.setItemMeta(next_meta);
					inv.setItem(53,next);
					p.openInventory(inv);
    		   }else{
    			   ItemMeta m = e.getCurrentItem().getItemMeta();
        		   List<String> lore = m.getLore();
        		   Bukkit.dispatchCommand(p, "hat " + lore.get(0));
        		   p.closeInventory();
    		   }
    		  

    	   }
       }
	}	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (TeamUtils.getTeamByPlayer(event.getPlayer()).isObserver() || !GameHandler.getGameHandler().getMatch().isRunning()) {
			if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				if (event.getPlayer().getItemInHand() != null) {
					if (event.getPlayer().getItemInHand().getType().equals(Material.NETHER_STAR)) {
						if (event.getPlayer().getItemInHand().hasItemMeta()) {
							if (event.getPlayer().getItemInHand().getItemMeta().hasDisplayName()) {
								if (ChatColor.stripColor(event.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase("atoms")) {
									Bukkit.dispatchCommand(event.getPlayer(), "atoms");
								}
							}
						}
					}
					if (event.getPlayer().getItemInHand().getType().equals(Material.STONE)) {
						if (event.getPlayer().getItemInHand().hasItemMeta()) {
							if (event.getPlayer().getItemInHand().getItemMeta().hasDisplayName()) {
								if (ChatColor.stripColor(event.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase("hats")) {
									Inventory inv = Bukkit.createInventory(null,54, "Hats");
									
									for(Material mat: HatCommand.page1){
										ItemStack stack = new ItemStack(mat);
										ItemMeta meta = stack.getItemMeta();
										List<String> lore = Arrays.asList(mat.name());
										meta.setLore(lore);
										stack.setItemMeta(meta);
										inv.addItem(stack);
										
								    }
									ItemStack next = new ItemStack(Material.ARROW);
									ItemMeta next_meta = next.getItemMeta();
									next_meta.setDisplayName("Go To Page 2");
									next.setItemMeta(next_meta);
									inv.setItem(53,next);
								    event.getPlayer().openInventory(inv);
								}
							}
						}
					}
					if (event.getPlayer().getItemInHand().getType().equals(Material.GOLDEN_APPLE)) {
						if (event.getPlayer().getItemInHand().hasItemMeta()) {
							if (event.getPlayer().getItemInHand().getItemMeta().hasDisplayName()) {
								if (ChatColor.stripColor(event.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase("trails")) {
									Inventory inv = Bukkit.createInventory(null,36, "Trails"); 
									for(Map.Entry<Effect,ItemStack> entry : TrailCommand.trails.entrySet()){
										ItemStack s = entry.getValue();
										ItemMeta m = s.getItemMeta();
										m.setDisplayName(entry.getKey().name());
										s.setItemMeta(m);
										inv.addItem(s);
									}
									ItemStack remove = new ItemStack(Material.ARROW);
									ItemMeta remove_meta = remove.getItemMeta();
									remove_meta.setDisplayName("Remove Trail");
									remove.setItemMeta(remove_meta);
									inv.setItem(35,remove );
									event.getPlayer().openInventory(inv);
								}
							}
						}
					}
				}
			}
		}
	}
}
