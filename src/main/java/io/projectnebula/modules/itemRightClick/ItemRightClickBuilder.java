package io.projectnebula.modules.itemRightClick;

import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class ItemRightClickBuilder implements ModuleBuilder {

	@Override
	public ModuleCollection<ItemRightClick> load(Match match) {
		return new ModuleCollection<ItemRightClick>(new ItemRightClick());
	}
}
