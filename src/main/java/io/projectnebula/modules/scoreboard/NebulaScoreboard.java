package io.projectnebula.modules.scoreboard;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.event.MatchStartEvent;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.module.modules.team.TeamModule;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.modules.kingofthehill.KOHTeam;

public class NebulaScoreboard implements Module {

	private static ArrayList<KOHTeam> teams;
	private ScoreboardManager manager;
	private Scoreboard board;
	private Objective obj;
	private double winningScore;
	
	public NebulaScoreboard(double winningScore){
		this.winningScore = winningScore;
		teams = new ArrayList<KOHTeam>();
		setupBoard();
	}
	
	public static ArrayList<KOHTeam> getTeams(){
		return teams;
	}
	
	public static boolean matchHasScoreboard() {
        return GameHandler.getGameHandler().getMatch().getModules().getModule(NebulaScoreboard.class) != null;
    }
	
	public static NebulaScoreboard getScoreboard(){
		return GameHandler.getGameHandler().getMatch().getModules().getModule(NebulaScoreboard.class);
	}
	
	@SuppressWarnings("rawtypes")
	public void addScore(KOHTeam team, double score){
		team.addScore(5.0);
		obj.getScore(team.getTeam().getColor() + team.getTeam().getName()).setScore((int)team.getScore());
		if(team.getScore() >= winningScore){
			try {
                TeamModule teamMod = team.getTeam();
                GameHandler.getGameHandler().getMatch().end(teamMod);
            } catch (IndexOutOfBoundsException ex) {
                GameHandler.getGameHandler().getMatch().end(null);
            }
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void loadTeams(){
		for(TeamModule team:TeamUtils.getTeams()){
			if(!team.getName().equals("Observers")){
				teams.add(new KOHTeam(team));
			}
		}
	}
	
	private void setupBoard(){
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		obj = board.registerNewObjective("test", "dummy");
		loadTeams();
		updateScoreboard();
	}
	
	private void updateScoreboard(){
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName("Team Scores:");
		for(KOHTeam team:teams){
			if(!team.getTeam().getName().equals("Observers")){
				obj.getScore(team.getTeam().getColor() + team.getTeam().getName()).setScore(0);
			}
		}
		for(Player pl:Bukkit.getServer().getOnlinePlayers()){
			pl.setScoreboard(board);
		}
	}
	
	@EventHandler
	public void onStart(MatchStartEvent e){
		teams.clear();
		setupBoard();
	}
	
	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

}
