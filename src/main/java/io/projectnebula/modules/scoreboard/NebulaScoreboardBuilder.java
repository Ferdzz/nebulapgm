package io.projectnebula.modules.scoreboard;

import org.jdom2.Element;

import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class NebulaScoreboardBuilder implements ModuleBuilder {

	@SuppressWarnings("rawtypes")
	@Override
	public ModuleCollection load(Match match) {
		ModuleCollection<NebulaScoreboard> results = new ModuleCollection<NebulaScoreboard>();
		for (Element board : match.getDocument().getRootElement().getChildren("nebulascore")) {
			double winningScore = 25;
			for (Element score : board.getChildren("winningscore")) {
				winningScore = Double.parseDouble(score.getText());
			}
			results.add(new NebulaScoreboard(winningScore));
		}
		return results;
	}

}
