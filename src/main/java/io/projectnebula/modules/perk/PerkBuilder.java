package io.projectnebula.modules.perk;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;
import io.projectnebula.modules.perk.perks.AssassinPerk;
import io.projectnebula.modules.perk.perks.GhostPerk;
import io.projectnebula.modules.perk.perks.RiddlerPerk;
import io.projectnebula.modules.perk.perks.ScavengerPerk;

import org.bukkit.Location;
import org.jdom2.Element;

public class PerkBuilder implements ModuleBuilder {

	@Override
	public ModuleCollection<Perk> load(Match match) {
		ModuleCollection<Perk> results = new ModuleCollection<Perk>();
		for (Element perks : match.getDocument().getRootElement().getChildren("perks")) {
			for (Element perk : perks.getChildren("riddler")) {
				for (Element loc : perk.getChildren("location")) {
					String[] coord = loc.getText().split(",");
					double x = Double.parseDouble(coord[0]);
					double y = Double.parseDouble(coord[1]);
					double z = Double.parseDouble(coord[2]);
					RiddlerPerk rid = new RiddlerPerk(new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z));
					results.add(rid);
				}
			}
			for (Element perk : perks.getChildren("ghost")) {
				for (Element loc : perk.getChildren("location")) {
					String[] coord = loc.getText().split(",");
					double x = Double.parseDouble(coord[0]);
					double y = Double.parseDouble(coord[1]);
					double z = Double.parseDouble(coord[2]);
					GhostPerk gh = new GhostPerk(new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z));
					results.add(gh);
				}
			}
			for (Element perk : perks.getChildren("scavenger")) {
				for (Element loc : perk.getChildren("location")) {
					String[] coord = loc.getText().split(",");
					double x = Double.parseDouble(coord[0]);
					double y = Double.parseDouble(coord[1]);
					double z = Double.parseDouble(coord[2]);
					ScavengerPerk scav = new ScavengerPerk(new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z));
					results.add(scav);
				}
			}
			for (Element perk : perks.getChildren("assassin")) {
				for (Element loc : perk.getChildren("location")) {
					String[] coord = loc.getText().split(",");
					double x = Double.parseDouble(coord[0]);
					double y = Double.parseDouble(coord[1]);
					double z = Double.parseDouble(coord[2]);
					AssassinPerk ass = new AssassinPerk(new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z));
					results.add(ass);
				}
			}
		}
		return results;
	}

}
