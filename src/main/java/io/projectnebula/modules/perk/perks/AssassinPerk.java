package io.projectnebula.modules.perk.perks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import io.projectnebula.modules.perk.Perk;

public class AssassinPerk extends Perk{

	public AssassinPerk(Location location) {
		super("Assassin", "Instant kill a player from behind!", location, new ItemStack(Material.IRON_SWORD), 10,  6 * 20 * 60);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onAttack(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player && e.getEntity() instanceof Player && getArrayOfPlayersWithPerk().contains((Player)e.getDamager())){
			Player player = (Player) e.getDamager();
			Player playerHit = (Player) e.getEntity();
			if(Math.abs(player.getLocation().getDirection().angle(playerHit.getLocation().getDirection())) <= 45){
				e.setDamage(20);
			}
		}
	}
	
}
