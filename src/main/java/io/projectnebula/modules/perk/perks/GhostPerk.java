package io.projectnebula.modules.perk.perks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import io.projectnebula.modules.perk.Perk;

public class GhostPerk extends Perk{

	public GhostPerk(Location location) {
		super("Ghost", "You are now invisible to others!", location, new ItemStack(Material.POTION), 5, 5 * 20 * 60);
	}
	
	@Override
	public void onEnable(Player p){
		for (Player players : Bukkit.getOnlinePlayers())
		{
			players.hidePlayer(p);
		}
	}
	
	@Override
	public void onDisable(Player p){
		for (Player players : Bukkit.getOnlinePlayers())
		{
			players.showPlayer(p);
		}
	}

}
