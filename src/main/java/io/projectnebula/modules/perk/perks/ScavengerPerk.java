package io.projectnebula.modules.perk.perks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import io.projectnebula.modules.perk.Perk;

public class ScavengerPerk extends Perk{

	public ScavengerPerk(Location location) {
		super("scavenger", "You now pick up double the items!",location, new ItemStack(Material.BONE), 15, 2*20*60);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPickUp(PlayerPickupItemEvent e){
		Player p = e.getPlayer();
		if(getArrayOfPlayersWithPerk().contains(p) && !e.getItem().hasMetadata("playerdrop") && !e.isCancelled()){
			p.getInventory().addItem(e.getItem().getItemStack());
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(getArrayOfPlayersWithPerk().contains(p)){
			Item item = e.getItemDrop();
			item.setMetadata("playerdrop", new FixedMetadataValue(pl, true));
		}
	}
}
