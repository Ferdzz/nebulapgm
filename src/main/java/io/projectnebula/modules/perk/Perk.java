package io.projectnebula.modules.perk;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.event.MatchStartEvent;
import in.twizmwaz.cardinal.module.Module;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.sk89q.minecraft.util.commands.ChatColor;

public class Perk implements Module {

	private ArrayList<Player> playersWithPerk;

	protected ItemStack floatingItem;
	protected Location location;
	protected String name;
	protected String desc;
	protected Plugin pl;
	protected long secondsLasted;
	protected long cooldown;
	protected boolean canPickup;

	public Perk(String name, String desc, Location location, ItemStack item, long secondsLasted, long cooldown) {
		this.name = name;
		this.desc = desc;
		this.location = location;
		this.floatingItem = item;
		this.playersWithPerk = new ArrayList<Player>();
		this.secondsLasted = secondsLasted;
		this.cooldown = cooldown;

		this.canPickup = true;
		this.pl = GameHandler.getGameHandler().getPlugin();
		
	}

	public ItemStack getFloatingItem() {
		return floatingItem;
	}

	public String getName() {
		return name;
	}

	public Location getLocation() {
		return location;
	}

	public void addPlayer(Player pl) {
		playersWithPerk.add(pl);
	}

	public void removePlayer(Player pl) {
		playersWithPerk.remove(pl);
	}

	public ArrayList<Player> getArrayOfPlayersWithPerk() {
		return playersWithPerk;
	}
	
	public void onEnable(Player p){}
	
	public void onDisable(Player p){}

	private void spawnItem() {
		HologramsAPI.createHologram(pl, location.add(0, 2, 0)).appendItemLine(floatingItem);
	}
	
	@EventHandler
	public void onPlayerStep(PlayerInteractEvent event) {
		final Player p = event.getPlayer();
		if (!getArrayOfPlayersWithPerk().contains(p) && event.getAction().equals(Action.PHYSICAL) && event.getClickedBlock().equals(location.getBlock()) && event.getClickedBlock().getType().equals(Material.GOLD_PLATE)) {
			if (canPickup) {
				addPlayer(p);
				p.sendMessage(ChatColor.AQUA + "You got the " + name + " perk!  " + ChatColor.RED + ChatColor.BOLD + desc);
				onEnable(p);
				
				// Perk cooldown
				canPickup = false;
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					@Override
					public void run() {
						canPickup = true;
					}
				}, cooldown);

				// Perk lasting time
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					@Override
					public void run() {
						removePlayer(p);
						p.sendMessage(ChatColor.AQUA + "The " + getName() + " perk has worn off!");
						onDisable(p);
					}
				}, secondsLasted * 20L);
			} else {
				p.sendMessage(ChatColor.AQUA + "Perk is on cooldown!");
			}
		}
	}

	@EventHandler
	public void onStart(MatchStartEvent event) {
		spawnItem();
	}

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

}
