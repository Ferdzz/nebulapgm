package io.projectnebula.modules.perk;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PerkItemListener implements Listener{

	@EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
		if(e.getItem().hasMetadata("unobtain")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onDespawn(ItemDespawnEvent e) {
		if(e.getEntity().hasMetadata("unobtain")){
			e.setCancelled(true);
		}
	}
}
