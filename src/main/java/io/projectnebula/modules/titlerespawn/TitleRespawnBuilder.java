package io.projectnebula.modules.titlerespawn;

import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class TitleRespawnBuilder implements ModuleBuilder {

	@SuppressWarnings("rawtypes")
	@Override
	public ModuleCollection load(Match match) {
		ModuleCollection<TitleRespawn> results = new ModuleCollection<TitleRespawn>();
		if(match.getDocument().getRootElement().getChild("titlerespawn") != null) {
			results.add(new TitleRespawn());
		}
		
		return results;
	}
}
