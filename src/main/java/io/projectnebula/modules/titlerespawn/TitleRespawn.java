package io.projectnebula.modules.titlerespawn;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.util.TeamUtils;

import java.util.ArrayList;
import java.util.Arrays;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class TitleRespawn implements Module {

	private ArrayList<Player> canRespawn = new ArrayList<Player>();

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			final Player player = (Player) event.getEntity();
			if (player.getHealth() - event.getDamage() <= 0 && !TeamUtils.getTeamByPlayer(player).isObserver()) {
				if (!canRespawn.contains(player)) {
					// Player 'dies'
					new Thread() {
						public void run() {
							player.setGameMode(GameMode.SPECTATOR);

							PlayerDeathEvent deathEvent = new PlayerDeathEvent(player, Arrays.asList(player.getInventory().getContents()), 0, 0, 0, 0, "%s died because of respawn");
							GameHandler.getGameHandler().getPlugin().getServer().getPluginManager().callEvent(deathEvent);

							TextComponent title = new TextComponent("You died!");
							title.setColor(ChatColor.RED);
							player.showTitle(title);
							for (int i = 5; i != 0; i--) {
								TextComponent sub = new TextComponent("Left click to respawn in: ");
								sub.setColor(ChatColor.GREEN);
								TextComponent sub2 = new TextComponent(i + "");
								sub2.setColor(ChatColor.AQUA);
								sub.addExtra(sub2);

								player.setSubtitle(sub);

								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							player.setTitleTimes(0, Integer.MAX_VALUE, 0);
							TextComponent sub = new TextComponent("Left click to respawn");
							sub.setColor(ChatColor.GREEN);
							player.showTitle(title);
							player.setSubtitle(sub);

							canRespawn.add(player);
						};
					}.start();
				}
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (canRespawn.contains(player) && (event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK))) {
			canRespawn.remove(player);
			player.setGameMode(GameMode.SURVIVAL);
			player.resetTitle();
		}
	}
}
