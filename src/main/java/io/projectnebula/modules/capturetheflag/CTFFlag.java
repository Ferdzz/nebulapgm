package io.projectnebula.modules.capturetheflag;

import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.block.Banner;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.util.TeamUtils;

public class CTFFlag {

	private Location spawnLoc;
	private Location loc;
	private String team;
	private DyeColor color;
	private Block bl;
	private Banner flag;
	private Player carrier;
	private boolean textDrawn;
	
	public CTFFlag(Location spawnLoc, String team){
		this.spawnLoc = spawnLoc;
		this.team = team;
		textDrawn = false;
		
		byte woolColor = (byte)0;	
		switch(team.toLowerCase().replace('_', ' ')){
			case "dark red":
				woolColor = (byte)14;
				break;
			case "red":
				woolColor = (byte)14;
				break;
			case "blue":
				woolColor = (byte)11;
				break;
			case "dark aqua":
				woolColor = (byte)9;
				break;
			case "aqua":
				woolColor = (byte)3;
				break;
			case "dark blue":
				woolColor = (byte)11;
				break;
			case "yellow":
				woolColor = (byte)4;
				break;
			case "gold":
				woolColor = (byte)1;
				break;
			case "dark green":
				woolColor = (byte)13;
				break;
			case "green":
				woolColor = (byte)5;
				break;
			case "dark purple":
				woolColor = (byte)10;
				break;
			case "light purple":
				woolColor = (byte)6;
				break;
			case "white":
				woolColor = (byte)8;
				break;
			case "gray":
				woolColor = (byte)8;
				break;
			case "dark gray":
				woolColor = (byte)7;
				break;
			case "black":
				woolColor = (byte)15;
				break;
			default:
				woolColor = (byte)0;
		}
		color = DyeColor.getByWoolData(woolColor);
	}
	
	public void removeFlag(){
		bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(loc);
		bl.setType(Material.AIR);
	}
	
	public void addFlag(Location location){
		if(GameHandler.getGameHandler().getMatchWorld().getBlockAt(location).getType()!=Material.AIR){
			bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(location);
			bl.setType(Material.STANDING_BANNER);
			flag = (Banner)bl.getState();
			flag.setBaseColor(color);
			flag.update();
			loc = location;
			if(!textDrawn){
				HologramsAPI.createHologram(GameHandler.getGameHandler().getPlugin(), new Location(spawnLoc.getWorld(), spawnLoc.getX(), spawnLoc.getY() + 3d, spawnLoc.getZ())).appendTextLine(TeamUtils.getTeamByName(team + " team").getColor() + team.substring(0, 1).toUpperCase() + team.substring(1) + " Team's Flag");
				textDrawn = true;
			}	
		}else{
			bl = GameHandler.getGameHandler().getMatchWorld().getBlockAt(spawnLoc);
			bl.setType(Material.STANDING_BANNER);
			flag = (Banner)bl.getState();
			flag.setBaseColor(color);
			flag.update();
			loc = location;
			if(!textDrawn){
				HologramsAPI.createHologram(GameHandler.getGameHandler().getPlugin(), new Location(spawnLoc.getWorld(), spawnLoc.getX(), spawnLoc.getY() + 3d, spawnLoc.getZ())).appendTextLine(TeamUtils.getTeamByName(team + " team").getColor() + team.substring(0, 1).toUpperCase() + team.substring(1) + " Team's Flag");
				textDrawn = true;
			}	
		}
	}
	
	public void capture(Player taker){
		ItemStack stack = flag.getMaterialData().toItemStack();
		stack.setType(Material.BANNER);
		stack.setAmount(1);
		
		BannerMeta meta = (BannerMeta)stack.getItemMeta();
		meta.setBaseColor(color);
		
		stack.setItemMeta(meta);
			
		taker.getInventory().setHelmet(stack);
		carrier = taker;
		removeFlag();
	}
	
	public void setCarrier(Player pl){
		carrier = pl;
	}
	
	public Player getCarrier(){
		return carrier;
	}
	
	public String getTeamName(){
		return team;
	}
	
	public Location getLocation(){
		return loc;
	}
	
	public Location getSpawnLocation(){
		return spawnLoc;
	}
	
}
