package io.projectnebula.modules.capturetheflag;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sk89q.minecraft.util.commands.ChatColor;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.event.MatchStartEvent;
import in.twizmwaz.cardinal.event.SnowflakeChangeEvent;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.module.modules.snowflakes.Snowflakes;
import in.twizmwaz.cardinal.util.MiscUtils;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.modules.scoreboard.NebulaScoreboard;

public class CaptureTheFlag implements Module {

	private ArrayList<CTFFlag> flags; 
	
	public CaptureTheFlag(ArrayList<CTFFlag> flags, double winningScore){
		this.flags = flags;
	}
	
	private void addFlags(){
		for(CTFFlag flag:flags){
			flag.addFlag(flag.getSpawnLocation());
		}
	}
	
	@EventHandler
	public void onStart(MatchStartEvent e){
		addFlags();
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		for(final CTFFlag flag:flags){
			if(flag.getCarrier() != null && flag.getCarrier().equals(e.getEntity())){
				e.getEntity().getInventory().setHelmet(null);
				flag.addFlag(e.getEntity().getLocation());
				flag.setCarrier(null);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(GameHandler.getGameHandler().getPlugin(), new Runnable() {
					@Override
					public void run() {
						flag.removeFlag();
						flag.addFlag(flag.getSpawnLocation());
					}
				}, 5 * 20L);
			}else{
				flag.addFlag(flag.getSpawnLocation());
			}
		}
	}
	
	@EventHandler
	public void onStep(PlayerMoveEvent e){	
		if(GameHandler.getGameHandler().getMatch().isRunning() && !TeamUtils.getTeamByPlayer(e.getPlayer()).getName().equals("Observers")){
			double x = Math.floor(e.getPlayer().getLocation().getX());
			double y = Math.floor(e.getPlayer().getLocation().getY());
			double z = Math.floor(e.getPlayer().getLocation().getZ());
			for(CTFFlag flag:flags){
				double flagX = Math.floor(flag.getLocation().getX());
				double flagY = Math.floor(flag.getLocation().getY());
				double flagZ = Math.floor(flag.getLocation().getZ());
				if(!flag.getTeamName().toLowerCase().equals(TeamUtils.getTeamByPlayer(e.getPlayer()).getName().toLowerCase().replace(" team", "")) && x==flagX && y==flagY && z==flagZ){
					flag.capture(e.getPlayer());
					Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent(e.getPlayer(), Snowflakes.ChangeReason.FLAG_PICKUP, 10, ""));
				}else if(flag.getCarrier() != null && flag.getCarrier().equals(e.getPlayer())){
					for(CTFFlag flag1:flags){
						flagX = Math.floor(flag1.getSpawnLocation().getX());
						flagY = Math.floor(flag1.getSpawnLocation().getY());
						flagZ = Math.floor(flag1.getSpawnLocation().getZ());
						if(flag1.getTeamName().toLowerCase().equals(TeamUtils.getTeamByPlayer(e.getPlayer()).getName().toLowerCase().replace(" team", "")) && x==flagX && y==flagY && z==flagZ){
							flag.addFlag(flag.getSpawnLocation());
							flag.setCarrier(null);
							e.getPlayer().getInventory().setHelmet(null);
							int index = -1;
							for(int team=0; team<NebulaScoreboard.getTeams().size(); team++){
								if(NebulaScoreboard.getTeams().get(team).getTeam().equals(TeamUtils.getTeamByName(flag1.getTeamName() + " Team"))){
									index = team;
									break;
								}
							}
							if(NebulaScoreboard.matchHasScoreboard()){
								NebulaScoreboard.getScoreboard().addScore(NebulaScoreboard.getTeams().get(index), 5.0);
							}
							e.getPlayer().sendMessage(ChatColor.RED + "You captured " + TeamUtils.getTeamByName(flag1.getTeamName() + " Team").getColor() + flag.getTeamName() + " team's " + ChatColor.RED + "flag!");
							Bukkit.getServer().getPluginManager().callEvent(new SnowflakeChangeEvent(e.getPlayer(), Snowflakes.ChangeReason.FLAG_CAPTURE, 20, ""));
							break;
						}
					}
				}
			}
		}
	}
	
	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

}
