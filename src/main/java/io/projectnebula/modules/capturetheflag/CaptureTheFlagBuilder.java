package io.projectnebula.modules.capturetheflag;

import java.util.ArrayList;

import org.bukkit.Location;
import org.jdom2.Element;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class CaptureTheFlagBuilder implements ModuleBuilder {

	@SuppressWarnings("rawtypes")
	@Override
	public ModuleCollection load(Match match) {
		ModuleCollection<CaptureTheFlag> results = new ModuleCollection<CaptureTheFlag>();
		for (Element ctf : match.getDocument().getRootElement().getChildren("capturetheflag")) {
			ArrayList<CTFFlag> flags = new ArrayList<CTFFlag>();
			for (Element locs : ctf.getChildren("flagloc")) {
				String[] coord = locs.getText().split(",");
				String teamName = locs.getAttributeValue("teamname");
				double x = Double.parseDouble(coord[0]);
				double y = Double.parseDouble(coord[1]);
				double z = Double.parseDouble(coord[2]);
				flags.add(new CTFFlag(new Location(GameHandler.getGameHandler().getMatchWorld(), x, y, z), teamName));
			}
			double winningScore = 25;
			for (Element score : ctf.getChildren("winningscore")) {
				winningScore = Double.parseDouble(score.getText());
			}
			results.add(new CaptureTheFlag(flags, winningScore));
		}
		return results;
	}

}
