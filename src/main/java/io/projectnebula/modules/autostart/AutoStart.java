package io.projectnebula.modules.autostart;

import in.twizmwaz.cardinal.GameHandler;
import in.twizmwaz.cardinal.event.MatchEndEvent;
import in.twizmwaz.cardinal.event.PlayerChangeTeamEvent;
import in.twizmwaz.cardinal.match.MatchState;
import in.twizmwaz.cardinal.module.Module;
import in.twizmwaz.cardinal.module.ModuleCollection;
import in.twizmwaz.cardinal.module.modules.startTimer.StartTimer;
import in.twizmwaz.cardinal.module.modules.team.TeamModule;
import in.twizmwaz.cardinal.util.ChatUtils;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.util.ColorUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.meta.FireworkMeta;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

public class AutoStart implements Module {

	@Override
	public void unload() {
		HandlerList.unregisterAll(this);
	}

	@SuppressWarnings("rawtypes")
	@EventHandler
	public static void autoStart(PlayerChangeTeamEvent event) {
		ModuleCollection<TeamModule> teams = TeamUtils.getTeams();
		boolean start = true;
		for (TeamModule team : teams) {
			if (!team.isObserver() && team.isEmpty()) {
				start = false;
			}
		}

		if (start && !GameHandler.getGameHandler().getMatch().getState().equals(MatchState.STARTING)) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "start 30");
		} else if (GameHandler.getGameHandler().getMatch().getState().equals(MatchState.STARTING) && !start) {
			GameHandler.getGameHandler().getMatch().setState(MatchState.WAITING);
			GameHandler.getGameHandler().getMatch().getModules().getModule(StartTimer.class).setCancelled(true);
			ChatUtils.getGlobalChannel().sendMessage(ChatColor.RED + "Match start countdown cancelled because " + event.getPlayer().getName() + ChatColor.RED + " changed team.");
		}
	}

	@EventHandler
	public static void autoCycle(MatchEndEvent event) {
		PacketContainer title = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.TITLE);
    	title.getIntegers().write(0, 0);
    	
    	if(event.getTeam() != null){
        	title.getChatComponents().write(0, WrappedChatComponent.fromText(event.getTeam().getColor() + event.getTeam().getName() + " won the game!"));
    	}else{
        	title.getChatComponents().write(0, WrappedChatComponent.fromText(ChatColor.LIGHT_PURPLE + "Nobody won the game!"));
    	}

    	for(Player pl:Bukkit.getOnlinePlayers()){
    		try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(pl, title);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
    	}
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!TeamUtils.getTeamByPlayer(player).isObserver()) {
				TeamUtils.getTeamByName("observer").add(player);
			}
		}

		// Used two fors because we want to have everyone as observer before shooting the fireworks
		for (Player player : Bukkit.getOnlinePlayers()) {
			Firework firework = (Firework) player.getWorld().spawn(player.getLocation(), Firework.class);
			FireworkMeta meta = firework.getFireworkMeta();
			Random rand = new Random();
			int fireworkType = rand.nextInt(5) + 1;
			
			Type type = null;
			switch (fireworkType) {
			case 1:
				type = Type.BALL;
				break;
			case 2:
				type = Type.BALL_LARGE;
				break;
			case 3:
				type = Type.BURST;
				break;
			case 4:
				type = Type.CREEPER;
				break;
			case 5:
				type = Type.STAR;
				break;
			default:
				type = Type.BALL;
				break;
			}
			
			FireworkEffect fireworkEffect = FireworkEffect.builder().trail(rand.nextBoolean()).flicker(rand.nextBoolean()).withColor(ColorUtils.getRandomColor(rand)).withFade(ColorUtils.getRandomColor(rand)).with(type).build();
			meta.addEffect(fireworkEffect);
			meta.setPower(rand.nextInt(2) + 1);
			firework.setFireworkMeta(meta);
		
			player.playSound(player.getLocation(), Sound.WITHER_DEATH, 1, 1);
		}
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "cycle 30");
	}
}
