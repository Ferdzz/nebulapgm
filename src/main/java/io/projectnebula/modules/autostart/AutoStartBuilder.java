package io.projectnebula.modules.autostart;

import in.twizmwaz.cardinal.match.Match;
import in.twizmwaz.cardinal.module.ModuleBuilder;
import in.twizmwaz.cardinal.module.ModuleCollection;

public class AutoStartBuilder implements ModuleBuilder {

	@SuppressWarnings("rawtypes")
	@Override
	public ModuleCollection load(Match match) {
		return new ModuleCollection<>(new AutoStart());
	}

}
