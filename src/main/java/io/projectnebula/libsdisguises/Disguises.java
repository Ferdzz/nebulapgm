package io.projectnebula.libsdisguises;

import me.libraryaddict.disguise.disguisetypes.DisguiseType;

public class Disguises {

	public static DisguiseType disguises[] = {DisguiseType.WOLF, DisguiseType.BAT, DisguiseType.CHICKEN
		    , DisguiseType.COW, DisguiseType.MUSHROOM_COW, DisguiseType.PIG
			, DisguiseType.RABBIT, DisguiseType.SHEEP, DisguiseType.SQUID, DisguiseType.VILLAGER
			, DisguiseType.CAVE_SPIDER, DisguiseType.ENDERMAN, DisguiseType.SPIDER, DisguiseType.PIG_ZOMBIE
			, DisguiseType.BLAZE, DisguiseType.CREEPER, DisguiseType.ELDER_GUARDIAN, DisguiseType.ENDERMITE
			, DisguiseType.GHAST, DisguiseType.GUARDIAN, DisguiseType.MAGMA_CUBE, DisguiseType.SILVERFISH
			, DisguiseType.SKELETON, DisguiseType.SLIME, DisguiseType.WITCH, DisguiseType.WITHER_SKELETON
			, DisguiseType.ZOMBIE, DisguiseType.ZOMBIE_VILLAGER};
	
	public static boolean isDisguiseAllowed(DisguiseType disguise){
		for(DisguiseType dis:disguises){
			if(dis==disguise){
				return true;
			}
		}
		return false;
	}
	
	public static boolean isDisguiseAllowed(String name){
		for(DisguiseType dis:disguises){
			if(name.toUpperCase().equals(dis.toString().toUpperCase())){
				return true;
			}
		}
		return false;
	}

}
