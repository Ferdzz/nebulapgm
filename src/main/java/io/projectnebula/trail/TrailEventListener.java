package io.projectnebula.trail;

import in.twizmwaz.cardinal.util.TeamUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class TrailEventListener implements Listener{

	@EventHandler
	public void onPlayerMoveEvent(PlayerMoveEvent event){
		for(TrailPlayer pl:TrailList.trailList){
			if(pl.getPlayer() == event.getPlayer() && (event.getFrom().getX() != event.getTo().getX() || event.getFrom().getZ() != event.getTo().getZ())){
				for(Player player:Bukkit.getServer().getOnlinePlayers()){
					if(TeamUtils.getTeamByPlayer(player).isObserver()){
						pl.playEffect(player);
					}						
				}
			}
	    }
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event){
		for(TrailPlayer pl:TrailList.trailList){
			if(pl.getPlayer() == event.getPlayer()){
				TrailList.trailList.remove(pl);
			}
	    }
	}
	
}
