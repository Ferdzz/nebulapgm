package io.projectnebula.trail;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TrailPlayer {

	private UUID uuid;
	private Effect effect;
	private int data;
	
	public TrailPlayer(UUID uuid, Effect effect, int data){
		this.effect = effect;
		this.data = data;
		this.uuid = uuid;
	}
	
	@SuppressWarnings("deprecation")
	public void playEffect(Player player){
		Player pl = Bukkit.getPlayer(uuid);

		player.playEffect(new Location(pl.getWorld(), pl.getLocation().getX(), pl.getLocation().getY() + 1, pl.getLocation().getZ()), effect, data);
	}
	
    public Player getPlayer(){
    	return Bukkit.getPlayer(this.uuid);
    }
    
    public Effect getEffect(){
    	return effect;
    }
	
}
