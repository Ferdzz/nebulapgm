package io.projectnebula.util;

import java.util.Random;

import org.bukkit.Color;

public class ColorUtils {
	public static Color getRandomColor() {
		return getRandomColor(new Random());
	}
	
	public static Color getRandomColor(Random random) {
		int color = random.nextInt(17) + 1;
		switch(color) {
		default:
		case 1: return Color.AQUA;
		case 2: return Color.BLACK;
		case 3: return Color.BLUE;
		case 4: return Color.FUCHSIA;
		case 5: return Color.GRAY;
		case 6: return Color.GREEN;
		case 7: return Color.LIME;
		case 8: return Color.MAROON;
		case 9: return Color.NAVY;
		case 10: return Color.OLIVE;
		case 11: return Color.ORANGE;
		case 12: return Color.PURPLE;
		case 13: return Color.RED;
		case 14: return Color.SILVER;
		case 15: return Color.TEAL;
		case 16: return Color.WHITE;
		case 17: return Color.YELLOW;
		}
	}
}
