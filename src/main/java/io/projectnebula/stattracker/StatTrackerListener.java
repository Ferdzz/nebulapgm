package io.projectnebula.stattracker;

import in.twizmwaz.cardinal.Cardinal;
import in.twizmwaz.cardinal.util.NumUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sk89q.minecraft.util.commands.ChatColor;

public class StatTrackerListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		if(!StatTracker.doesDataBaseContain(e.getPlayer().getName())){
			e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "Welcome " + e.getPlayer().getName() + "!");
			StatTracker.addNewUser(e.getPlayer().getName());
		}else{
			Cardinal.getCardinalDatabase().put(e.getPlayer(), "snowflakes", StatTracker.getValue(e.getPlayer().getName(), "atoms") + "");
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		StatTracker.updateValue(e.getPlayer().getName(), "atoms", Integer.parseInt(Cardinal.getCardinalDatabase().get(e.getPlayer(), "snowflakes")));
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player killer = e.getEntity().getKiller();
		Player player = e.getEntity();
		
		int killerKills = -1;
		int playerDeaths = StatTracker.getValue(player.getName(), "deaths");
		
		if(killer != null){
			killerKills = StatTracker.getValue(killer.getName(), "kills");
			StatTracker.saveDeath(killer.getName(), player.getName());
		}
		
		if(killerKills != -1){
			StatTracker.updateValue(killer.getName(), "kills", killerKills+1);
		}
		
		if(playerDeaths != -1){
			StatTracker.updateValue(player.getName(), "deaths", playerDeaths+1);
		}
	
	}
	
}
