package io.projectnebula.stattracker;

import in.twizmwaz.cardinal.Cardinal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StatTracker {

	private static Connection con = null;
	private static PreparedStatement pst = null;
	private static Statement st = null;
	
	private static String url = "jdbc:mysql://projectnebula.io/mcstats";
	private static String user = "root";
	private static String password = "1ESfQsiW";
	
	public static void connect(){	
		try{
            con = DriverManager.getConnection(url, user, password);            
        }catch(SQLException ex){
            Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);          
        }
	}
	
	public static void addNewUser(String username){
        try{
        	if(con.isClosed()){
        		connect();
        	}
        	int atoms = 0;
        	if(Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(username), "snowflakes").equals("")){
        		atoms = 0;
        	}else{
        		atoms = Integer.parseInt(Cardinal.getCardinalDatabase().get(Bukkit.getOfflinePlayer(username), "snowflakes"));
        	}
			pst = con.prepareStatement("INSERT INTO PlayerStats(username, kills, deaths, atoms) VALUES(?, ?, ?, ?)");
	        pst.setString(1, username);
	        pst.setInt(2, 0);
	        pst.setInt(3, 0);
	        pst.setInt(4, atoms);
	        pst.executeUpdate();
		}catch (SQLException ex) {
			Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}finally{
            try {
                if (pst != null) {
                	pst.close();
                }
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(StatTracker.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
	}	
	
	public static void updateValue(String username, String item, int val){
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("UPDATE PlayerStats SET " + item + " = ? "
                    + "WHERE username = ?");
            pst.setInt(1, val);
            pst.setString(2, username);         
            pst.executeUpdate();
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(StatTracker.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
	}
	
	public static int getValue(String username, String item){
		int val = -1;
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("SELECT * FROM PlayerStats WHERE username = ?");
            pst.setString(1, username);         
  
            ResultSet rs = pst.executeQuery();
            
            while(rs.next()){
            	val = rs.getInt(item);
            }
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(StatTracker.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return val;
	}
	
	public static void saveDeath(String killer, String killed){
		try{
			if(con.isClosed()){
        		connect();
        	}
			pst = con.prepareStatement("INSERT INTO Kills(killer, killed) VALUES(?, ?)");
	        pst.setString(1, killer);
	        pst.setString(2, killed);
	        pst.executeUpdate();
		}catch (SQLException ex) {
			Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}finally{
            try {
                if (pst != null) {
                	pst.close();
                }
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(StatTracker.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
	}
	
	public static boolean doesDataBaseContain(String username){
		boolean val = false;
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("SELECT * FROM PlayerStats WHERE username = ?");
            pst.setString(1, username);         
  
            ResultSet rs = pst.executeQuery();
            
            if(rs.next()){
            	val = true;
            }
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(StatTracker.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(StatTracker.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return val;
	}
}
