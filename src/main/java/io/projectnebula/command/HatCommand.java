package io.projectnebula.command;

import in.twizmwaz.cardinal.chat.ChatConstant;
import in.twizmwaz.cardinal.chat.LocalizedChatMessage;
import in.twizmwaz.cardinal.util.ChatUtils;
import in.twizmwaz.cardinal.util.TeamUtils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

public class HatCommand {
	
	public static List<Material> page1 = new ArrayList<Material>();
	public static List<Material> page2 = new ArrayList<Material>();
	public static List<Material> page3 = new ArrayList<Material>();

	public static List<Material> blacklist = new ArrayList<Material>();
	public static void initHats(){
		blacklist.add(Material.WOOD_BUTTON);
		blacklist.add(Material.STONE_BUTTON);
		blacklist.add(Material.WOODEN_DOOR);
		blacklist.add(Material.ACACIA_DOOR);
		blacklist.add(Material.DARK_OAK_DOOR);
		blacklist.add(Material.JUNGLE_DOOR);
		blacklist.add(Material.SPRUCE_DOOR);
		blacklist.add(Material.BIRCH_DOOR);
		blacklist.add(Material.NETHER_WARTS);
		blacklist.add(Material.FLOWER_POT);
		blacklist.add(Material.BREWING_STAND);
		blacklist.add(Material.BED_BLOCK);
		blacklist.add(Material.REDSTONE_COMPARATOR_OFF);
		blacklist.add(Material.REDSTONE_COMPARATOR_ON);
		blacklist.add(Material.REDSTONE_WIRE);
		blacklist.add(Material.REDSTONE_TORCH_OFF);
		blacklist.add(Material.REDSTONE_TORCH_ON);
		blacklist.add(Material.TORCH);
		blacklist.add(Material.STANDING_BANNER);
		blacklist.add(Material.WALL_BANNER);
		blacklist.add(Material.CAULDRON);
		blacklist.add(Material.CARROT);
		blacklist.add(Material.CROPS);
		blacklist.add(Material.PUMPKIN_STEM);
		blacklist.add(Material.MELON_STEM);
		blacklist.add(Material.POTATO);
		blacklist.add(Material.CARROT);
		blacklist.add(Material.TRIPWIRE_HOOK);
		blacklist.add(Material.TRIPWIRE);
		blacklist.add(Material.CAKE_BLOCK);
		blacklist.add(Material.PISTON_EXTENSION);
		blacklist.add(Material.PISTON_MOVING_PIECE);
		blacklist.add(Material.LAVA);
		blacklist.add(Material.WATER);
		blacklist.add(Material.STATIONARY_LAVA);
		blacklist.add(Material.STATIONARY_WATER);
		blacklist.add(Material.HOPPER);
		blacklist.add(Material.LAVA);
		blacklist.add(Material.AIR);
		blacklist.add(Material.BURNING_FURNACE);
		blacklist.add(Material.SOIL);
		blacklist.add(Material.DOUBLE_PLANT);
		blacklist.add(Material.DOUBLE_STEP);
		blacklist.add(Material.DOUBLE_STONE_SLAB2);
		blacklist.add(Material.WOOD_DOUBLE_STEP);
		blacklist.add(Material.WOOD_STEP);
		blacklist.add(Material.LONG_GRASS);
		blacklist.add(Material.YELLOW_FLOWER);
		blacklist.add(Material.RED_ROSE);
		blacklist.add(Material.RED_MUSHROOM);
		blacklist.add(Material.BROWN_MUSHROOM);
		blacklist.add(Material.SNOW);
		blacklist.add(Material.SUGAR_CANE_BLOCK);
		blacklist.add(Material.COCOA);
		blacklist.add(Material.FIRE);
		blacklist.add(Material.IRON_FENCE);
		blacklist.add(Material.SIGN_POST);
		blacklist.add(Material.DIODE);
		blacklist.add(Material.DIODE_BLOCK_OFF);
		blacklist.add(Material.DIODE_BLOCK_ON);
		blacklist.add(Material.ENDER_PORTAL);
		
		int x=1;
		for(Material mat:Material.values()){
			if(!blacklist.contains(mat)){
				if(isStringBlock(mat.toString())){
					if(x<=52){
						page1.add(mat);
					}else if(x>52 && x<105){
						page2.add(mat);
					}else{
						page3.add(mat);
					}
					
					x++;
				}
			}
		}
	}

	@Command(aliases = { "hat" }, desc = "Gives you a hat", usage = "<hat>")
	public static boolean hat(CommandContext cmd, CommandSender sender) throws CommandException {
		if (sender instanceof Player && TeamUtils.getTeamByPlayer((Player) sender).isObserver()) {
			Player pl = (Player) sender;
			if (pl.hasPermission("nebula.hat")) {
				if (cmd.argsLength() == 0) {
					listHats(sender);
					return true;
				} else {
					String hatString = cmd.getString(0);
					if (isStringBlock(hatString)) {
						pl.getInventory().setHelmet(new ItemStack(Material.getMaterial(hatString.toUpperCase())));
						sender.sendMessage("§7§lYou are now using the "+ hatString.toLowerCase() + " hat");
						return true;
					} else {
						sender.sendMessage(ChatColor.RED + "Invalid hat!");
						return true;
					}
				}
			} else {
				throw new CommandException(ChatUtils.getWarningMessage(ChatColor.RED + new LocalizedChatMessage(ChatConstant.GENERIC_PLEASE_DONATE, "").getMessage(((Player)sender).getLocale())));
			}
		}
		return false;
	}
	
	private static void listHats(CommandSender sender){
		String msg = "";
		for(Material mat:Material.values()){
			if(isStringBlock(mat.toString())){
				msg+=ChatColor.AQUA + mat.toString().toLowerCase() + ChatColor.RED + ", ";
			}
		}
		sender.sendMessage(msg);
	}
	
	private static boolean isStringBlock(String string){
		Material mat = Material.getMaterial(string.toUpperCase());

		if(mat == null){
			return false;
		}else{
			return Material.getMaterial(string.toUpperCase()).isBlock();
		}
	}
	
}
