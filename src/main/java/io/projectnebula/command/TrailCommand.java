package io.projectnebula.command;

import in.twizmwaz.cardinal.chat.ChatConstant;
import in.twizmwaz.cardinal.chat.LocalizedChatMessage;
import in.twizmwaz.cardinal.util.ChatUtils;
import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.trail.TrailList;
import io.projectnebula.trail.TrailPlayer;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

public class TrailCommand {

	public static Map<Effect,ItemStack> trails = new HashMap<Effect,ItemStack>();
    public static void initTrails(){
    	 trails.put(Effect.ENDER_SIGNAL,new ItemStack(Material.ENDER_PEARL));
    	 trails.put(Effect.MOBSPAWNER_FLAMES,new ItemStack(Material.BLAZE_POWDER));
    	 trails.put(Effect.FIREWORKS_SPARK,new ItemStack(Material.FIREWORK));
    	 trails.put(Effect.CRIT,new ItemStack(Material.FIREWORK_CHARGE));
    	 trails.put(Effect.MAGIC_CRIT,new ItemStack(Material.SPIDER_EYE));
    	 trails.put(Effect.POTION_SWIRL,new ItemStack(Material.POTION));
    	 trails.put(Effect.SPELL,new ItemStack(Material.FEATHER));
    	 trails.put(Effect.INSTANT_SPELL,new ItemStack(Material.BONE));
    	 trails.put(Effect.WITCH_MAGIC,new ItemStack(Material.CAULDRON_ITEM));
         trails.put(Effect.NOTE,new ItemStack(Material.NOTE_BLOCK));
         trails.put(Effect.PORTAL,new ItemStack(Material.PORTAL));
         trails.put(Effect.FLYING_GLYPH,new ItemStack(Material.ENCHANTMENT_TABLE));
         trails.put(Effect.FLAME,new ItemStack(Material.BLAZE_ROD));
         trails.put(Effect.LAVA_POP,new ItemStack(Material.LAVA));
         trails.put(Effect.SPLASH,new ItemStack(Material.WATER_BUCKET));
         trails.put(Effect.PARTICLE_SMOKE,new ItemStack(Material.BREWING_STAND_ITEM));
         trails.put(Effect.EXPLOSION,new ItemStack(Material.TNT));
         trails.put(Effect.VOID_FOG,new ItemStack(Material.BEDROCK));
         trails.put(Effect.COLOURED_DUST,new ItemStack(Material.WOOL));
         trails.put(Effect.SNOWBALL_BREAK,new ItemStack(Material.SNOW_BALL));
         trails.put(Effect.SLIME,new ItemStack(Material.SLIME_BALL));
         trails.put(Effect.HEART,new ItemStack(Material.REDSTONE));
         trails.put(Effect.VILLAGER_THUNDERCLOUD,new ItemStack(Material.FIREBALL));
         trails.put(Effect.HAPPY_VILLAGER,new ItemStack(Material.EMERALD));
    }
	
	
	@Command(aliases = { "trail" }, desc = "Adds a particle trail", usage = "<particle>")
	public static boolean trail(CommandContext cmd, CommandSender sender) throws CommandException {
		if(sender instanceof Player && TeamUtils.getTeamByPlayer((Player)sender).isObserver()){
			Player pl = (Player)sender;
			if(pl.hasPermission("nebula.trail")) {
				if(cmd.argsLength()==0){
					sender.sendMessage(ChatColor.AQUA + "Available effects:");
					sender.sendMessage(ChatColor.RED + listTrails());
					return true;
				}
				
				if(cmd.argsLength()==1 && cmd.getString(0).equalsIgnoreCase("remove")){
					removeTrail(sender);
					return true;
				}
	
				removeTrail(sender);
	
				for(int x=15; x<=Effect.values().length-3; x++){
					if(Effect.values()[x].toString().equals(cmd.getString(0).toUpperCase().replace(' ', '_'))){
						Effect effect = Effect.valueOf(cmd.getString(0).toUpperCase().replace(' ', '_'));
						TrailList.trailList.add(new TrailPlayer(pl.getUniqueId(), effect, 1));
						sender.sendMessage("§7§lYou are now using the " + effect.getName() + " trail");
						return true;
					}
				}
				sender.sendMessage(ChatColor.AQUA + "Specified trail effect" + ChatColor.RED + " not " + ChatColor.AQUA + "found!");
				sender.sendMessage(ChatColor.AQUA + "Available effects:");
				sender.sendMessage(ChatColor.RED + listTrails());
				return true;
			} else {
				throw new CommandException(ChatUtils.getWarningMessage(ChatColor.RED + new LocalizedChatMessage(ChatConstant.GENERIC_PLEASE_DONATE, "").getMessage(((Player)sender).getLocale())));
			}
		}
		return false;
	}
	
	private static void removeTrail(CommandSender sender){
		for(TrailPlayer trailPl:TrailList.trailList){
			if(trailPl.getPlayer() == (Player)sender){
				sender.sendMessage(ChatColor.RED + "Trail removed!");
				TrailList.trailList.remove(trailPl);
				break;
			}
		}
	}
	
	private static String listTrails(){
		String msg = "";
		for(int x=15; x<=Effect.values().length-3; x++){
			msg += ChatColor.RED + Effect.values()[x].toString().toLowerCase() + ChatColor.AQUA + ", ";
		}
		return msg;
	}
}
