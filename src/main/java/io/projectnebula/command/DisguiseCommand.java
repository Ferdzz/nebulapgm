package io.projectnebula.command;

import in.twizmwaz.cardinal.util.TeamUtils;
import io.projectnebula.libsdisguises.Disguises;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.ChatColor;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class DisguiseCommand {
	
	@Command(aliases = { "disguise", "d" }, desc = "Disguises you as a mob", usage = "<mob>")
	@CommandPermissions("nebula.disguise")
	public static boolean disguise(CommandContext cmd, CommandSender sender){
		if(sender instanceof Player && TeamUtils.getTeamByPlayer((Player)sender).isObserver()){
			if(cmd.argsLength()==0){
				sender.sendMessage(ChatColor.RED + "Disguises:");
				listDisguises(sender);
				return true;
			}
			
			DisguiseType disguise = null;
			
			if(Disguises.isDisguiseAllowed(cmd.getString(0))){
				disguise = DisguiseType.valueOf(cmd.getString(0).toUpperCase());
			}
			
			if(disguise != null && Disguises.isDisguiseAllowed(disguise)){
				DisguiseAPI.disguiseToAll((Player)sender, new MobDisguise(disguise));
				sender.sendMessage(ChatColor.AQUA + "Disguise added!");
				return true;
			}else{
				sender.sendMessage(ChatColor.RED + "Invalid/Disallowed mob");
				return true;
			}
			
		}
		return false;
	}
	
	@Command(aliases = { "undisguise", "u" }, desc = "Undisguises you")
	@CommandPermissions("nebula.disguise")
	public static boolean undisguise(CommandContext cmd, CommandSender sender){
		if(sender instanceof Player && TeamUtils.getTeamByPlayer((Player)sender).isObserver()){
			DisguiseAPI.undisguiseToAll((Player)sender);
			sender.sendMessage(ChatColor.AQUA + "Undisguised!");
			return true;
		}
		return false;
	}
	
	private static void listDisguises(CommandSender sender){
		String msg = "";
		for(DisguiseType dis:Disguises.disguises){
			msg+=ChatColor.AQUA + dis.toString().toLowerCase() + ChatColor.RED + ", ";
		}
		sender.sendMessage(msg);
	}
}
