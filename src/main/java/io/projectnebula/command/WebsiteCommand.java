package io.projectnebula.command;

import org.bukkit.command.CommandSender;

import com.sk89q.minecraft.util.commands.ChatColor;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;

public class WebsiteCommand {
	@Command(aliases = {"website", "register"}, desc = "Leads to the Project Nebula website")
	public static void onCommand(CommandContext cmd, CommandSender sender) {
		sender.sendMessage(ChatColor.GOLD + "http://www.projectnebula.io");
	}
}
