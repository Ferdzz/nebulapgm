package io.projectnebula.oretoblock;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class OreToBlockListener implements Listener {

	@EventHandler
	public void onMine(BlockBreakEvent e) {

		Player player = e.getPlayer();
		Block block = e.getBlock();

		if (block.getType() == Material.COAL_ORE) {

			ItemStack oreBlock = new ItemStack(Material.COAL_BLOCK, 1);
			player.getInventory().addItem(oreBlock);
			player.sendMessage("§bYay, no more ores!");

		} else if (block.getType() == Material.IRON_ORE) {

			ItemStack oreBlock = new ItemStack(Material.IRON_BLOCK, 1);
			player.getInventory().addItem(oreBlock);
			player.sendMessage("§bYay, no more ores!");

		} else if (block.getType() == Material.GOLD_ORE) {

			ItemStack oreBlock = new ItemStack(Material.GOLD_BLOCK, 1);
			player.getInventory().addItem(oreBlock);
			player.sendMessage("§bYay, no more ores!");

		} else if (block.getType() == Material.DIAMOND_ORE) {

			ItemStack oreBlock = new ItemStack(Material.DIAMOND_BLOCK, 1);
			player.getInventory().addItem(oreBlock);
			player.sendMessage("§bYay, no more ores!");

		}

	}

}
