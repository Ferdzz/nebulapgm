##TO DO LIST

- Implement double jump to all TDM and Blitz maps.

- Make NebulaPGM track every players deaths, kills, k/d ratio and who killed who. We need this to work with MYSQL as we will be integrating it with our website. **IMPORTANT**

- (FFA) No teams, every player for themselves. Spawns are defined in XML files and first person to a certain amount of points wins. (Usually 30 kills.)

- Change "Hat" to Hats and "Trail" to Trails, also change the trails color to &b **[Done]**

- Change "no permission" message when players try to use hats and trails to the donate for this feature message. (donate for this feature message is already implemented in cardinal, so just use that same one.) **[Done]**

- Add perks to maps?????

- Fix the double jump XP bar so it works smoother **[Done]**, and add some sort of &l&a colored notification to let users know that their jump suit is ready.

- Change selected trails and hats message to "&l&7You are now using the (trail or hat name) trail/hat"

- Change "Blitz" gamemode name to Fury

- TNT Defuser keeps moving around the inventory on different maps. **[Done]** *Fixed prior to this commit*

- On game end, add this sound: mob.wither.death, pitch 1.0 **[Done]**

- **GAMEMODE** At the start of the game, a team is picked, this can be map specific. The team that is picked, gets a pig in a minecart and they have to push it all the way along a track to the other teams base or area, then it explodes and the game ends. The other team has to defend its area and try to kill the pig or make sure that time runs out. Once the pig reaches the other teams designated area or block, it explodes. (Look up TF2 Payload for more info.)

- At the end of the game, the losing team loses all of their items (except for armor) and get slowness 2, and the winning team has one hit kill for 15 seconds to slaughter their enemies. (Add explosions and what not for fun)